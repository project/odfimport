
Drupal.behaviors.odfimport = function(context){
  $("#edit-submit").click(function(){
    if ($('#edit-body').val()) {
      if ($('#edit-importfile').val() != '' && $('#edit-body').val() != '') {
        answer = confirm('You have chosen to import an ODF file. This will overwrite the content body. Do you want to continue ?');
        if (answer) {
          return true;
        }
        else{
          return false;
        }
      }
    }
  });

};
